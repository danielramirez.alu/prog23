// PRIMER EXEMPLE D'INTERFÍCIES FUNCIONALS (CONSUMER) I EXPRESSIONS LAMBDES

// Exemple que carrega de Strings un objecte Vector i els mostra per pantalla
import java.util.*;
import java.util.function.*;

public class consumer
{
	public static void main(String args[])
	{
		Vector<String> vector = new Vector<String>();
		
		vector.add("Sergio");
		vector.add("");
		vector.add("Daniel");
		vector.add("Roberto");
		vector.add("Jorge");
		vector.add("");
		System.out.println("El contingut del objecte Vector és:");
		// 1. Amb bucle foreach
		for(String s: vector)
			System.out.println(s);
		System.out.println("El contingut del objecte Vector és:");
		// 2. Amb iterator
		Iterator<String> it = vector.iterator();
		while (it.hasNext())
			System.out.println(it.next());
		System.out.println("El contingut del objecte Vector és:");
		// 3. Amb el mètode foreach() que usa una interfície funcional (resolta amb expressió lambda)
		/* la sintaxi serà:
			InterficieFuncional if = lambda; */
		Consumer<String> cons = (s) -> { System.out.println(s); };
		vector.forEach(cons);	
			
		/* Esborrarem aquells elements del Vector que estiguen buits
		//Predicate<String> predicat = (s) -> { s.isEmpty() }; 
		Predicate<String> predicat = s ->  s.isEmpty();
		vector.removeIf(predicat);*/
		
		// Esborrar elements que començen per 'D'
		Predicate<String> predicat = s ->  s.startsWith("D");
		vector.removeIf(predicat);
		
		// Torne a mostrar el vector i comprove que han desaparegut els elements buïts
		System.out.println("El contingut del objecte Vector és:");
		vector.forEach(cons);
	}
}
