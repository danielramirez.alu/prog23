
class rectangle implements Cloneable
{
	private int ample;
	private int alt;
	private String nom;
	
	public rectangle(int an,int al) { ample=an; alt=al; }
	public void incrementarample() { ample++; }
	public void incrementaralt() { alt++; }
	public void setnom(String s) { nom=s; }
	public int getample() { return ample; }
	public int getalt() { return alt; }
	public String getnom() { return nom; }
	public Object clone()
	{
        	Object objeto=null;
        	try{
            		objeto =super.clone();
        	}
        	catch(CloneNotSupportedException ex){
            		System.err.println(" Errada al clonar");
        	}
	        return objeto;
	}
	public String toString() { return "Ample: " + ample + ", alt: "+ alt + ",nom: " +nom;}
}

public class testeoclone
{
	public static void main(String[] args)
	{
		rectangle r1 = new rectangle(5,7);
		rectangle r2 = (rectangle) r1.clone();
		r2.incrementarample();
		r2.incrementaralt();
		r1.setnom("Xicotet");
		r2.setnom("Gran");
		System.out.println(r1);
		System.out.println(r2);
    	}
}

