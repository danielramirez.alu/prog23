package ocells;

public class Colibri extends Ocell implements Cloneable
{
	private String color;
	
	public Colibri (double lon, String col) {super(lon);color=col;}

	public Colibri (Colibri c)
	{ 
		super(c.longitudBec);
		color=c.color;
	}

	public void setColor (String col) {color=col;}
	public String getColor () {return color;}

	public void pia (){super.pia(); super.pia();}
	public void vola(){System.out.println("Estoy volando a 30 km/h");}
	public void cova(){System.out.println("Date la vuelta estoy poniendo huevos");}
	@Override
	public Object clone() throws CloneNotSupportedException
	{
		if (this != null)
			return (Colibri) super.clone();
		else
			return null;
	}
	public String toString () {return "Logitud del bec: "+longitudBec+" cms\t Color: "+color;}
}
