import java.util.*;

public class e7
{
	public static void main(String args[])
	{
		Scanner ent = new Scanner(System.in);
		Vector<Integer> vector = new Vector<Integer>();
		System.out.println("Introduix un enter positiu ( 0 o negatiu per a acabar):");
		int num = ent.nextInt();
		while (num > 0)
		{
			vector.add(num);
			System.out.println("Introduix un enter positiu ( 0 o negatiu per a acabar):");
			num = ent.nextInt();
		}
		vector.forEach(n -> { int i=1; int max =1;
					 while(i < n)
					 {
					  	if ((n % i) == 0)
					  		max = i;
					  	i++;
					  }
					  System.out.println(max);
				}); 
	}

}
