package motors;

public abstract class Motor implements potencia
{
	public static double velocidad;
	public Motor()
	{
		velocidad=1500;
	}
	public Motor(double vel)
	{
		velocidad=vel;
	}
	public Motor(Motor m)	// constructor de còpia
	{
		velocidad=m.velocidad;
	}
	public static void setVelocidad(double vel)
	{
		velocidad=vel;
	}
	public static double getVelocidad()
	{
		return velocidad;
	}
	public abstract void acelera();
	@Override
	public void gira()
	{
		System.out.println("El motor gira a "+velocidad+" rpm");
	}
}


