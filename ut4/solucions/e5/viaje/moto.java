package viaje;

public class moto extends vehiculo
{
    private String tipo;

    public moto(double consumo, double potencia, String tipo)
    {
        super(consumo,potencia);
        this.tipo=tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo=tipo;
    }
    public String getTipo()
    {
        return tipo;
    }

    public String toString()
    {
        return "La moto del tipo "+tipo+" tiene una potencia de "+potencia+" y un consumo de "+consumo+" litros por km";
    }
    @Override
    public double consumViatge(double km)
    {
        //por cada km se cobran 20€
        return km*10;
    }
}
