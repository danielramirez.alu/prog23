import java.util.*;

public class ex8 {
	public static void main(String[] args) {
		if (args.length < 4)
			System.out.println("El programa se usa así \"java programa dia mes año Unidad (segundos, dias, anios)\"");
		else 
		{
			Integer anio = Integer.valueOf(args[2]);
			Integer mes = Integer.valueOf(args[1])-1;
			Integer dia = Integer.valueOf(args[0]);
			String variable = args[3].toLowerCase();
			Calendar calendar = new GregorianCalendar();
			//Date actual = new Date();
			//calendar.setTime(actual);
			Calendar calendarioUsuario = new GregorianCalendar(anio,mes,dia);
			if (calendar.compareTo(calendarioUsuario) > 0)
			{	
				long diferenciaEnTiempo = calendar.getTimeInMillis() - calendarioUsuario.getTimeInMillis();
				switch (variable)
				{
					case "segundos":
						
							diferenciaEnTiempo = diferenciaEnTiempo / 1000;
							System.out.println("Han pasado "+diferenciaEnTiempo+" segundos.");
							break;
						
					case "dias":
						
							diferenciaEnTiempo = diferenciaEnTiempo / (1000 * 60 * 60 * 24);
							System.out.println("Han pasado "+diferenciaEnTiempo+ " días.");
							break;
						
					case "anios":
						
							Calendar c = Calendar.getInstance();
							c.setTimeInMillis(diferenciaEnTiempo);
							int anios = c.get(Calendar.YEAR)-1970;
							System.out.println("Han pasado "+anios+" años.");
							break;
						
				}
			}		
			else
			{
				System.out.println("Fecha futura (no válida)");
			}
		}
	}
}
