// El nostre primer programa MODULAR ...

public class holaDam1
{
    public static void main(String args[])
    {
    	//int i;
       // ús de la funció
       for (int i=1; i < 11 ; i++)	// i és variable local no a tota main, sino local al for (hem reduït el seu àmbit)
       	saluda();
       //System.out.println(i); // no puc accedir a la variable i, estic fora del seu àmbit

    }
    
    // definició de la funció
    public static void saluda()
    {
    	System.out.println("Hola DAM1");
    }
}

