// Exemple d'exepció

import java.util.*;
/*import java.util.Scanner;
import java.util.InputMismatchException;*/

public class excepcio1
{
	public static void main(String args[]) //throws NumberFormatException
	{
		Scanner ent = new Scanner(System.in);
		double num=0;
		
		System.out.println("Introduix un número:");
		try
		{
			//num = Double.parseDouble(System.console().readLine());
			num = ent.nextDouble();
		}
		catch(InputMismatchException e)
		{
			System.err.println("Ha de ser un valor numèric");

		}
		finally
		{
			System.out.println("Fi del programa");
		}	
			
		
		//System.out.println(num);
	}
}
