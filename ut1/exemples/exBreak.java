/* Exemple de break per a forçar l'eixida d'un bucle

    Programa que demane números positius fins a acabar amb un negatiu */

public class exBreak
{
    public static void main(String args[])
    {
        double num;

        while (true)
        {
            System.out.println("Introduix un número positiu (negatiu per a acabar):");
            num = Double.parseDouble(System.console().readLine());
            if (num < 0)
                break;
        }
    }
}
