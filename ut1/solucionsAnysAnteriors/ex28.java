/* 28*. Els nombres de Fibonacci són els membres d'una seqüència en la qual cada número és igual a la suma dels dos anteriors. En altres paraules, Fi = Fi-1 + Fi-2, on Fi és l'i-èsim nombre de Fibonacci, i sent F1 = F2 = 1. Per tant:

    F5 = F4 + F3 = 3 + 2 = 5, i així successivament.

Escriure un programa que mostre els n primers nombres de Fibonacci, sent n un valor introduït per teclat. NACHO */

import java.util.Scanner;

public class ex28
{
public static void main(String[] args) 
{
	Scanner ent = new Scanner(System.in);
	System.out.println("Por favor introduce un número");//25
	int suma, num = ent.nextInt();
	int cont=2;
	int num2=0;
	int num3=1;
	if  (num > 0)
	{
		if (num == 1)
			System.out.print("La serie fibonacci: \n 0 ");
		else if (num >= 2)
			{
				System.out.print("La serie fibonacci: \n 0 1 ");
				while(cont < num)
				{
					suma=num2+num3;
					cont++;
					System.out.print(suma + " ");
					num2 = num3;
					num3 = suma;
				}
			}
	}
	System.out.println("");
}
}
