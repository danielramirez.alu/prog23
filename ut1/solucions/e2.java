//2. Programa que demana 2 valors numèrics i mostra el resultat de la seva suma i el seu producte.

public class e2
{ 
    public static void main(String args[])
    {
        // result no és necessària, la utilitzem per a donar legibilitat al codi
        double num1,num2,result;
        
        System.out.println("introduce 2 valores numericos ");
        num1= Double.parseDouble(System.console().readLine());
        num2= Double.parseDouble(System.console().readLine());
        result= num1*num2;
        System.out.println("el resultado de la suma es " + (num1+num2));
        System.out.println("el resultado de su producto es " + result);       
    }
}


