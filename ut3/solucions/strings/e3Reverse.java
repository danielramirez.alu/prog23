// 3. Implemente un programa que indique si una paraula és un palíndrome . Una paraula és palíndrome si es llig igual d'esquerra a dreta que de dreta a esquerra.

import java.util.Scanner;

public class e3Reverse
{
	public static void main(String args[])
	{
		String s;
		Scanner sc = new Scanner(System.in);
		
		// Leemos el String
		System.out.print("Introdueix una paraula per a comprobar si és un palíndrome: ");
		s = sc.nextLine();

		// Creamos un StringBuffer para poder darle la vuelta
		StringBuffer sb = new StringBuffer(s);
		sb.reverse();
		
		// Lo pasamos a String para poder compararlos y ver si son iguales
		String s1 = sb.substring(0);
		
		/* Asignamos los valores de s y s1 a las variables s2 y s3 y las pasamos a minúsculas para comparar ambos Strings sin diferenciar entre mayúsculas y minúsculas.
		String s2 = s.toLowerCase();
		String s3 = s1.toLowerCase(); */
		
		if(s.equals(s1))
			System.out.println("La paraula " + s + " és un palíndrome.");
		else
			System.out.println("La paraula " + s + " no és un palíndrome.");	
	}
}

