class Disc{

	private String titol;
	private String grupic;
	private double preu = 15;

	// CONSTRUCTOR PER DEFECTE
	public Disc(){
		titol = "Revolver";
		grupic = "The Beatles";
		//preu = 15;
	}
	// constructor GENERAL
	public Disc(String t, String gm){
		titol = t;
		grupic = gm;
	}
	
	public Disc(Disc d){
		titol = d.titol;
		grupic = d.grupic;
	}

	public void setTitol(String t) { titol = t; }
	public void setGrupic(String g) { grupic = g; }
	public String getTitol() { return titol; }
	public String getGrupic() { return grupic; }
	public double getPreu() { return preu; }
	
	public double ofertaDisc(){
		preu = preu - preu * 0.2;	// preu = 0.8*preu;
		return preu;
	}
	 
	public void mostraDisc(){
		System.out.println("\nTítol: " + titol + "\nGrup o Music: " + grupic + "\nPreu: " + preu + "€");		
	}
	
	// toSring
	public String toString()	// Forma d'ús: System.out.println(d1);
	{
		return "\nTítol: " + titol + "\nGrup o Music: " + grupic + "\nPreu: " + preu + "€";
	}
	 
}

/*
	for (int i= 0 ; i < array.length ; i++)
		System.out.println(array[i]);	// System.out.println(array[i].toString());*/
		

public class e2
{
	public static void main(String args[])
	{
		int opc;
		
		// creem l'array de Discos
		Disc ds[] = new Disc[MIDA];
		
		do
		{ System.out.println("1. Alta de disc
		2. Mostrar tots els discos
		3. Modifificar disc existent
		4. Ordenar per preu
		0. Eixir ");
			System.out.println("Trie opció:");
			// llegir opció
			opc = ...
			switch (opcio)
			{
				case 1: // demanar titol i grup i cridar a alta() 
					break;
				case 2: // cridar a un mètode llista()
				
				...
				
				default:
			}
		} while (true);
						
		
		

	}
}
