// exemple de cerca d'un valor dins d'un array (recerca ENTERA) MÚLTIPLE: retornant totes les aparicions

import java.util.Scanner;

public class CercaLinialArrayMultiple
{
	private static Scanner ent = new Scanner(System.in);
	
	public static void main(String args[])
	{
		final int N = 5; double valor; int pos[];
		
		pos = new int[N];
		// inicialitze l'array de posicions amb tots els valors iguals a 0 
		for (int i=0 ; i < pos.length ; i++)
			pos[i] = 0;
		// creem l'array de 4 elements
		double nums[] = new double[N];
		// carregar valors des de teclat
		carregaValors(nums);
		// mostrar valors de l'array en pantalla
		mostraValors(nums);
		// demanaré un valor a cercar
		System.out.println("Introduix un valor de recerca:");	
		valor = ent.nextDouble();
		pos = cerca(nums,valor);
		for (int i=0 ; i < pos.length ; i++)
			if (pos[i] > 0)
				System.out.println("El valor " + valor + " s'ha trobat a la posició " + pos[i]);
			
	}
	
	public static void carregaValors(double nums[])
	{
		for (int i=0 ; i < nums.length ; i++)
		{
			System.out.println("Introduix un valor numèric:");
			nums[i] = ent.nextDouble();
		}
	}
	
	public static void mostraValors(double nums[])
	{
		for (int i=0 ; i < nums.length ; i++)
			System.out.print("\tPosició " + (i + 1) + ": " + nums[i]);
		System.out.println("");
	}
	
	public static int[] cerca(double nums[], double v)
	{
		int posicions[], j=0;
		
		posicions = new int[nums.length];	// int posicions[] = new int[nums.length];
		
		for (int i=0 ; i < nums.length ; i++)
			if (nums[i] == v)
			{
				posicions[j] = i+1;
				j++;
			}
		return posicions;
	}
}
