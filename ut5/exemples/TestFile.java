// import java.io.File;
// import java.util.Date;

public class TestFile {
	public static void main (String [] args) {
	
		java.io.File f = new java.io.File("image/imatge.jpg");
		
		System.out.println ( "Existeix: " + f.exists ());
		System.out.println ( "té una mida de " + f.length () + "bytes");
		System.out.println ( "Pot ser llegit? " + f.canRead());
		System.out.println ( "Pot ser escrit? " + f.canWrite());
		System.out.println ( "Pot ser executat? " + f.canExecute());
		System.out.println ( "És un directori? " + f.isDirectory ());
		System.out.println ( "És un arxiu? " + f.isFile());
		System.out.println ( "És absolut? " + f.isAbsolute ());
		System.out.println ( "està ocult? " + f.isHidden ());
		System.out.println ( "La ruta absoluta és " + f.getAbsolutePath ());
		System.out.println ( "Ultima modificació: " + new java.util.Date(f.lastModified ()));
		// Farem que mostre també el contingut del subdirectori image
		
		java.io.File f2 = new java.io.File("image");
		String fitxers[] = f2.list();
		System.out.println("El contingut del subdirectori és:");
		for( String s : fitxers)
			System.out.println(s);
			
			
	}
}
