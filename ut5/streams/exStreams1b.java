// Exemple que sume els positius des d'un array de doubles
import java.util.*;

public class exStreams1b
{
	public static void main(String[] args) {
		int nums[] = {7,2,-3,0,13,-8,9,10};

		// Generar un stream a partir d'un array
		long positius = Arrays.stream(nums).filter(n->n>=0).sum();	// Ara sumem amb el mètode sum()
		System.out.println("La suma dels positius és " + positius );
	}
}
