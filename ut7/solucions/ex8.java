/* 8. Crea un programa visual que permeta afegir discos a la taula d'igual nom.

Un botó permetrà la connexió a la base de dades, i l'altre donar d'alta cada disc segons el valor dels seus camps. */
import java.sql.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class ex8
{
	//DEFINICIÓ DE LES REFERÈNCIES COM A OBJECTES.
	private static JFrame jf; //Finestra principal del programa.
	private static JMenuBar menuBar; //MenuBar
	private static JMenu menu1; //Menu Archiu
	private static JMenu menu2; //Menu Opcions
	private static JMenuItem menuItem; //MenuItem
	private static JPanel jp1; //Primer panell del programa.
	private static JPanel jp2; //Segon panell del programa que contindrà botons.
	private static JPanel jp1Titol; //Panell que contindrà el label i jtfield.
	private static JPanel jp1Artista; //Panell que contindrà el label i jtfield.
	private static JPanel jp1Preu; //Panell que contindrà el label i jtfield.
	private static JLabel jlTitol; //Label de Nom.
	private static JLabel jlArtista; //Label de Data de ingrés.
	private static JLabel jlPreu; //Label de Data de ingrés.
	private static JTextField jtfTitol; //Text field de Nom.
	private static JTextField jtfArtista; //Text field de Data de ingrés.
	private static JTextField jtfPreu; //Text field de Salari.
	private static JButton jb; //Botó de agregar.


	public static void main(String[] args)
	{
		//FINESTRA
			//Creem la finestra.
			jf = new JFrame("Agrega Venedors");
			//DEfinim un layout per a la finestra.
			jf.setLayout(new GridLayout(0,1));
			//Fem que el programa acabe al tancar la finestra.
			jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			//Centrem la finestra.
			jf.setLocationRelativeTo(null);
			//Fem visible la finestra.
			jf.setVisible(true);

		//MENU
			//Creem els objectes del menu.
			menuBar = new JMenuBar();
			menu1 = new JMenu("Archiu");
			menu2 = new JMenu("Opcions");
			menuItem = new JMenuItem("Eixir");
			//Agreguem els objectes a la finestra.
			menu2.add(menuItem);
			menuBar.add(menu1);
			menuBar.add(menu2);
			jf.setJMenuBar(menuBar);

		//PANELL
			//Creem el panell amb un layout.
			jp1 = new JPanel(new GridLayout(1,0));
			jp1Titol = new JPanel(new GridLayout(0,1));
			jp1Artista = new JPanel(new GridLayout(0,1));
			jp1Preu = new JPanel(new GridLayout(0,1));
			jp2 = new JPanel(new GridLayout(1,0));
			//Afegim els panells a la finestra.
			jp1.add(jp1Titol);
			jp1.add(jp1Artista);
			jp1.add(jp1Preu);
			jf.getContentPane().add(jp1);
			jf.getContentPane().add(jp2);

		//LABELS
			//Creem els labels.
			jlTitol = new JLabel("TITOL");
			jlArtista = new JLabel("ARTISTA");
			jlPreu = new JLabel("PREU");

		//TEXT FIELDS
			//Creem els JTextFields.
			jtfTitol = new JTextField();
			jtfArtista = new JTextField();
			jtfPreu = new JTextField();
			//Afegim els elements als corresponents panells.
			jp1Titol.add(jlTitol);
			jp1Titol.add(jtfTitol);
			jp1Artista.add(jlArtista);
			jp1Artista.add(jtfArtista);
			jp1Preu.add(jlPreu);
			jp1Preu.add(jtfPreu);

		//BOTÓ
			//creem el botó.
			jb = new JButton("AGREGAR");
			//Agefim el botó al segón panell.
			jp2.add(jb);

		//CONTROL D'ESDEVENIMENTS
			//Creem els ActionListeners per al boto.
			ActionListener alb = e -> {accions(e);};
			ActionListener alm = e -> {System.exit(0);};
			//Afegim el ActionListener al botó.
			jb.addActionListener(alb);
			menuItem.addActionListener(alm);

		//EXTRAS
			//Fem que el tamany de la finestra s'adecue al minim dels elements.
			//jf.pack();
			//Posem un tamany mínim a la finestra.
			jf.setSize(400,200);


	}

	public static void accions(ActionEvent e)
	{
		String sql,titol,artista,preu;
		titol=jtfTitol.getText();
		artista=jtfArtista.getText();
		preu=jtfPreu.getText();
		try(Connection con = DriverManager.getConnection("jdbc:mysql://localhost/musica","root","");
			Statement stmt = con.createStatement();)
		{
			System.out.println("Connecting Database...");
			sql="INSERT INTO discos VALUES (2,"+"'"+titol+"'"+","+"'"+artista+"',"+preu+")";
			stmt.executeUpdate(sql);
			System.out.println(sql);
			System.out.println("Row inserted.");
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		}

	}
}
